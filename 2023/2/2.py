with open('./input.txt', 'r') as file:
    input = file.read().strip()

rows = input.split('\n')
output = 0

i = 0
while i < len(rows):
    row = rows[i]
    game_id = int(row.split(': ')[0].split(' ')[1])
    subsets = row.split(': ')[1].split('; ')

    minimal_bag = {
        'red': 0,
        'green': 0,
        'blue': 0
    }

    j = 0
    while j < len(subsets):
        set = subsets[j]
        elements = set.split(', ')

        k = 0
        while k < len(elements):
            cubes = int(elements[k].split(' ')[0])
            color = elements[k].split(' ')[1]

            if cubes > minimal_bag[color]:
                minimal_bag[color] = cubes

            k += 1
        j += 1

    game_power = 1
    for c in minimal_bag:
        game_power *= minimal_bag[c]

    output += game_power

    i += 1

print(output)
