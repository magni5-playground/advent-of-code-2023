with open('./input.txt', 'r') as file:
    input = file.read().strip()

rows = input.split('\n')
output = 0

bag = {
    'red': 12,
    'green': 13,
    'blue': 14
}

i = 0
while i < len(rows):
    row = rows[i]
    game_id = int(row.split(': ')[0].split(' ')[1])
    subsets = row.split(': ')[1].split('; ')

    is_game_possible = True

    j = 0
    while j < len(subsets):
        set = subsets[j]
        elements = set.split(', ')

        k = 0
        while k < len(elements):
            cubes = int(elements[k].split(' ')[0])
            color = elements[k].split(' ')[1]

            if cubes > bag[color]:
                is_game_possible = False

            k += 1
        j += 1

    if is_game_possible:
        output += game_id

    i += 1

print(output)
