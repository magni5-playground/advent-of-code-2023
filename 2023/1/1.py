def is_digit(char):
    return char >= '0' and char <= '9'
    
    
with open('./input.txt', 'r') as file:
    input = file.read().strip()

rows = input.split('\n')
totalSum = 0

i = 0
while i < len(rows):
    row = rows[i]
    firstDigit = -1
    lastDigit = -1
    j = 0
    while j < len(row):
        if (is_digit(row[j])):
            if (firstDigit == -1):
                firstDigit = row[j]
            lastDigit = row[j]
        j += 1
    totalSum += int(firstDigit + '' + lastDigit)
    i += 1

print(totalSum)
