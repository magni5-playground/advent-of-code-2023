def is_digit(char):
    return char >= '0' and char <= '9'

def spelled_to_digit(x):
    x = x.replace('one', 'o1e')
    x = x.replace('two', 't2o')
    x = x.replace('three', 't3e')
    x = x.replace('four', 'f4r')
    x = x.replace('five', 'f5e')
    x = x.replace('six', 's6x')
    x = x.replace('seven', 's7n')
    x = x.replace('eight', 'e8t')
    x = x.replace('nine', 'n9e')
    return x


with open('./input.txt', 'r') as file:
    input = file.read().strip()

rows = input.split('\n')
totalSum = 0

i = 0
while i < len(rows):
    row = rows[i]
    row = spelled_to_digit(row)
    firstDigit = -1
    lastDigit = -1
    j = 0
    while j < len(row):
        if is_digit(row[j]):
            if firstDigit == -1:
                firstDigit = row[j]
            lastDigit = row[j]
        j += 1
    totalSum += int(firstDigit + '' + lastDigit)
    i += 1

print(totalSum)